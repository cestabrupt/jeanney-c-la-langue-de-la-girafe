---
author: "C. Jeanney"
title: "La langue de la girafe"
publisher: "Abrüpt"
date: "janvier 2019"
lang: "fr"
rights: "© 2019 Abrüpt, CC BY-NC-SA"
---

# {-}

*Elle saisit le feuillage d’une façon très singulière, faisant sortir pour cet effet une langue longue, rugueuse, très étroite et noire, en l’entortillant autour de l’objet qu’elle convoite.*
Étienne Geoffroy Saint-Hilaire, *Quelques Considérations sur la Girafe*, 1827.

# {-}

Bon mes marqueurs mes pinceaux.

Les musées royaux exhibent de symboliques tableaux.

Les visiteurs peuvent également voir les dernières œuvres de l'artiste.

Une structure d'aluminium censée durer six mois n'a pas été démolie.

De la pluie sur les vitres, un escalator, un escalier rouge, des
lumières, des pommes vertes géantes sur lesquelles il est possible de s'asseoir.

Des icônes, des symboles reviennent sans arrêt, constamment, sans arrêt
déclinés, reviennent constamment et interpellent : l'artiste est mort, vive l'artiste.

Je vois des choses qui me plaisent.

Je vois des choses qui me déplaisent.

La rivière des parfums est la raison pour laquelle une ville fut
établie.

C'est donc au bord de cette rivière que j'entame ma quête.

Je vais rencontrer ces habitants et voir comment ils vivent, et voir où
ils vivent.

Je serai votre guide.

Par moment, quelques bateaux remonteront le courant.

J'ai hâte de commencer ce voyage.

Pittoresque par temps clair, agité par temps sombre, la rivière
représente souvent l'âme de la ville.

Je peux entendre les mouches voler, dit-il.

C'est d'un calme impressionnant.

Musique traditionnelle.

Drapeau rouge flottant sur fond de ciel gris.

Dates, kilométrages, le guide tient un carnet qu'il n'ouvre pas, mais
puisqu'un crayon semble coincé au milieu des pages on peut supposer qu'il s'en sert ou qu'il va s'en
servir.

Je suis curieux de voir ce qu'il se passe à l'intérieur, dit-il.

Il présente un guide qui va devenir son guide.

Il nous guide grâce à un guide.

Dites-m'en un peu plus sur la géomancie.

Dates, kilométrages.

Quelles sont leurs significations ?

À droite le dragon, à gauche le tigre blanc, tous deux protègent la
ville.

C'est l'entrée principale ? demande-t-il au guide.

Vous pouvez m'en dire plus ? demande-t-il au guide.

Quelle est la signification du lotus, alors que l'empereur avançait sur
ce majestueux pont ?

Le bâtiment est fidèle à sa promesse, dit-il.

Dorures, colonnes, or, statues, tables ouvragées, sculptures, c'est une
histoire merveilleuse.

Anniversaires royaux et autres cérémonies d'importance.

Cela donne un sentiment de magnificence, avec des détails exquis.

C'est aussi inspirant que spectaculaire.

On ne peut ressentir que de l'admiration, dit-il.

Tous menaient une vie majestueuse.

En plein milieu nous avons le trône.

De nombreux empereurs (treize) se sont assis sur ce trône.

On ne peut nier le sentiment d'opulence et de grandeur lorsqu'on se
tient sous le porche blanc décoré de plantes grimpantes et de vignes sculptées.

Une porte ouverte donne sur un totem de pierre, moussu et recouvert de
formes géométriques.

Dites-m'en plus, demande-t-il au guide.

Chaque arbre du verger est entouré d'un cercle de feuilles mortes.

Le guide qui lui sert de guide est une femme.

Lui s'appuie sur une colonne noire, du même noir que l'écharpe autour de
son cou.

La femme guide porte une robe traditionnelle violette parée de broderies
rouges.

Lorsqu'il avance, il se déplace vers nous.

La guide en robe violette se déplace latéralement.

Elle ne regarde que lui, ne s'adresse qu'à lui.

Plan large avec des jonques, suivi d'un travelling sur un escalier
large.

Il s'est placé au centre pour s'adresser à nous.

La caméra est derrière lui pendant qu'il monte l'escalier.

Fondu enchaîné.

Il pleut sur les bâtiments de pierre.

La femme guide, en robe traditionnelle violette, s'adresse à lui.

De dos, on peut voir ses longs cheveux s'étendre jusqu'à sa ceinture.

Elle montre une tête de dragon sculptée et des soldats sculptés, à
taille pratiquement humaine.

Il est sous un parapluie.

Musique traditionnelle.

Elle monte l'escalier à ses côtés, tenant sa jupe longue de ses deux
mains.

Deux hérons se font face.

Il est facile de deviner une forte influence dans ce mausolée, dit-il.

Il est facile de deviner l'influence coloniale.

Ses murs sont joliment décorés de mosaïque.

Il s'agit d'une œuvre d'art d'une très grande qualité.

On y voit le printemps, l'été, l'automne et l'hiver.

Vous êtes dans la salle principale, dit-elle.

L'empereur est enterré en dessous.

C'est un cadre vraiment somptueux, dites-m'en plus.

La verrière est suspendue à un crochet.

La statue est recouverte de feuilles d'or.

Pour moi c'est incontestablement la pièce la plus spectaculaire de ce
complexe, dit-il.

J'ai vu une ville ancienne qui a survécu à la modernité.

Un rappel vivant que la beauté est éternelle.

À partir d'un geste primitif brut, je souligne la singularité de chaque
personnage.

Il y a ce photographe, je m'en souviens, qui va photographier un parc.

Plus tard il examine sa photographie.

Plus il zoome dans l'image et plus il remarque des détails, et
finalement un corps mort.

Plus on zoome, plus on a des révélations, mais plus on zoome et plus on
est confronté aux détails et aux corps morts, et plus on est confronté à
la matière.

Les abysses sont si vastes qu'il est impossible de les explorer
entièrement et d'en comprendre les significations.

Arborescences, fractales, coraux, algues, une forêt de motifs et d'êtres
microscopiques.

Le spectateur est immergé.

Cela se génère aléatoirement et à l'infini.

Les vibrations, oui, les vibrations de nos corps, pour vibrer au tempo
de la nature.

Une vibration, une réflexion sur la fragilité de ce milieu et la
nécessité de le préserver.

Des personnages énigmatiques se tiennent avec fierté et confiance.

On parle maintenant du grand architecte des barres.

Il construit un ensemble emblématique, entre prouesse technique et
poésie urbaine.

Site d'exception.

C'est un monstre sacré, de par l'importance de son œuvre construite et
théorique.

Un style nouveau qui étonne ou qui choque.

Il conçoit en fonction de l'utilité, se basant sur l'homme qui mesure un
mètre quatre-vingt-trois, dit la femme.

Et ça se manifeste absolument partout, dit une autre femme.

C'est vraiment le plus flagrant.

C'est ce qui crève les yeux.

L'idée étant de se baser sur l'homme, dit la femme.

L'idée étant d'économiser les matériaux.

On aboutit à un quartier vertical.

Une extrême économie d'espace, en termes de hauteur, en termes de
déplacements.

C'est extrêmement ingénieux dans sa conception.

On aurait pu avoir des doutes sur cette histoire, mais non.

Elle était répétée en permanence.

C'est pour ça qu'on parle d'empire.

On en revient toujours à ces lieux qui fabriquent l'identité permanente.

Poètes et hommes politiques ont tenté d'adoucir les légendes.

Ou de les remplacer par des récits plus aseptisés.

Il existe donc plusieurs versions.

Ils suppriment la partie qui a sans doute la plus grande emprise sur les
esprits.

Ils inventent d'autres récits bien plus lisses, bien plus adaptés à la
création de mythes fondateurs.

Ils offrent un récit plus glorieux.

Naturellement ça n'a rien d'historique.

C'est la violence qui se cache derrière le mythe.

Une sérigraphie moderne que l'on pratique aujourd'hui dans les
observatoires.

Celui-ci est un ancien bagne converti en laboratoire.

Ici commence un patient travail de classification.

Je vais essayer de le remettre dans le champ là.

Tu seras prêt ?

C'est un monde extrêmement complet, très dynamique, qu'on commence à
décrire.

Quelque part on a hérité des plus anciens animaux du monde, évidemment.

Fouiller ce monde pourrait un jour révolutionner notre vie.

Le voyage reprend.

Nous embarquons dans un nouveau prototype.

Mais les dangers qui nous attendent sont encore plus remarquables.

Il faut descendre dans un bouillon de feu.

On se fraie un chemin.

On crée des îles.

L'activité est incessante.

Quand on arrive, la nuit éclaire tout le ciel.

La même chose a lieu sous la mer.

C'est notre destination.

Cette première partie est facile.

Les océanographes et les géologues cherchent des signes.

On dirait Mars.

Il n'y a que des cailloux.

On suppose qu'il y aura une série d'habitats transitionnels.

Remontez le panier.

Entendu.

On s'attendrait presque à voir des gnous.

Ce qui fait la spécificité de notre planète, c'est la présence de la
mer.

La jeune fille décide de rester.

Elle dit : non, moi je ne partirai pas.

Mon bonheur est ici.

La relève est déjà prête.

L'eau des sources vient des profondeurs de la terre.

Elle soigne les rhumatismes et le stress.

Seigneur Dieu, j'ai jamais bu autant d'eau de ma vie.

On va faire circuler l'eau.

Elle arrive ici et elle est distribuée sur tous les postes de soin.

Les conduites en plomb, c'est de l'histoire ancienne.

Ce lieu se devait d'avoir une symbolique forte.

Et pour cause.

La vue est imprenable.

Le site est superbe, les pieds dans l'eau.

Elle est écrivaine.

Elle a voulu revenir aux sources.

Avec elle, nous allons découvrir des savoir-faire ancestraux.

On va l'envoyer, la faire filer, elle va nous revenir sous forme de
bobines et de palettes.

Dès qu'on tire dessus, c'est très cassant.

L'effort est de taille.

Ça va assez vite.

Pour l'arrêter on tourne dans l'autre sens.

Avant, ça marchait avec des chardons végétaux.

On a un pouvoir d'adaptation relativement rapide.

On répond presque immédiatement aux besoins.

C'est ce qui explique notre survie.

Un endroit unique pour des rencontres au sommet. Les chardons nous surveillent.

Ce lieu, c'est le fil rouge de sa vie.

Revenir ici, c'est forcément ouvrir la boîte à souvenir.

Je me revois bondir, rebondir d'un côté à l'autre.

C'est la culture de ce pays.

Ce qui fait apprendre aux hommes à travailler ensemble, à se serrer les
coudes.

Il reste une icône.

Une source de motivation.

C'est beaucoup d'émotion pour lui.

Trente hommes dans un vestiaire.

Se tapent dans les mains.

S'échangent des maillots.

Se tapent sur la nuque.

Musique dansante.

Applaudissements.

Victoire.

On va pouvoir boire une bière tranquille.

Il n'est pas le dernier à mettre de l'ambiance.

C'est une élite.

Étape par étape.

Chaque jour est un défi à leurs limites.

Chaque jour est un pas de plus.

Ils nous accueillent avec des sourires.

Le soir on dort en même temps, c'est vraiment magnifique.

On mange ensemble.

C'est comme une famille.

J'espère que ce jour sera mon jour.

Pour certains, ce sera une nuit blanche.

C'est le point d'orgue avec une progression de nuit et des souvenirs
gravés à jamais dans l'esprit de tous.

Il n'y a pas de hasard.

C'est une recette cent pour cent naturelle.

C'est encore plus de proximité.

L'allié idéal.

Roulez en liberté.

Libre de vivre.

De goûter à des expériences inédites.

Libre d'écouter la mer.

Ou le silence.

On a finalisé le dossier.

Réagir peut tout changer.

C'est conçu pour découvrir des voies.

On pourra s'y mettre tout de suite.

Mais on repère une nouvelle menace.

Si elle se détache et tombe sur la voie, on ne peut pas prendre de
risques.

La nuit quand la température baisse, l'eau commence à geler.

La glace s'accumule couche après couche.

Elle forme une stalactite de plus en plus grosse.

Jusqu'à devenir un véritable danger.

Ma plus grande inquiétude.

Ancien magicien professionnel, il est dans sa première année de
formation.

Cela exige un grand dévouement.

Ce n'est pas un métier, c'est un mode de vie.

Je compte rester un moment.

La glace bloque le passage dans le tunnel de la passe de l'aigle.

Les gens y allaient avec des pioches et ils creusaient à la main.

Beaucoup de gens sont morts ici.

Il pourrait y avoir des blessés.

C'est un vrai danger qu'on a là.

Faut qu'on dégage ces colonnes.

À la sortie du tunnel, il y a une colonne de glace.

Celle-ci aurait pu s'effondrer toute seule.

On a le feu vert.

Je vérifie les points pour être sûr qu'il n'y a pas d'obstruction.

Je l'ai appris à mes dépens.

J'ai perdu le respect des autres.

Je vérifie qu'il n'y a pas de fissures, de fragments ou d'entailles.

Voilà notre réserve d'explosifs.

On va s'attaquer à quelques corniches et peut-être à quelques pentes.

Il faut un gros déclencheur.

C'est une sorte de réaction en chaîne.

Reste plus qu'à attendre le train.

Les glaciologues étudient la montagne et l'évolution des glaciers, ils
sont six en anoraks orange.

Tendent des filins pour s'entraîner sur des parois d'escalade.

Ils portent des bandanas.

Des shorts sanglés de crochets.

Il y a un certain romantisme, avec des histoires de camarades plus
expérimentés.

Puis le romantisme s'en va et il s'agit de connaître ses capacités et
ses limites.

De participer à des expéditions.

Nous sommes capables de monter très haut, nous n'avons pas besoin
d'oxygène.

Dans cette équipe, il y a deux femmes, glaciologues elles aussi.

Au campement, elles font chauffer de l'eau et brassent de la farine,
mélangent des ingrédients tout en parlant de glaciation et de mesures techniques, pendant que les
hommes boivent quelque chose dans des timbales en fer blanc à l'extérieur.

Je ne dois prononcer aucune mauvaise parole, dit la femme qui habite à
flanc de montagne.

Elle fait le tour d'un mausolée, construit en un seul jour par les
paysans d'ici.

Des envahisseurs sont venus pour le détruire, ils ont fait tomber les
pierres scellées tout en haut et en sont morts, décapités.

Ce mausolée rend les femmes fécondes.

Le poète leur disait comment élever leurs enfants.

Et tout ce qu'il disait se réalisait, il était pour nous un oracle, dit
la femme.

Dans sa maison, au milieu de nulle part, elle rassemble des objets
qu'elle a collectés : ici des ornements d'habits féminins, un bracelet en argent, des coraux pour
orner les coiffures, des décorations pour les chevaux, pour le harnachement de parade du cheval,
un oreiller avec une broderie d'œil de loup pour protéger les nouveau-nés.

Dans la seule pièce chauffée, elle garde les photos de ses voyages.

En costume national, elle a participé à des conférences d'ethnologues.

L'histoire la plus célèbre est celle du trésor perdu.

Deux cents dromadaires portent un trésor, mais les caravaniers
apprennent qu'il va y avoir un guet-apens.

Le trésor est caché, les témoins tous tués.

On descend un chemin de pierre.

On fait des fouilles.

Un homme a repris les recherches.

Trois forages de plusieurs mètres de profondeur.

Il y a des études géophysiques.

À trente mètres sous le niveau du sol, on repère une masse métallique
anormale.

C'est compliqué de l'atteindre.

Le campement est au pied de la falaise.

Ici, autrefois, on a découvert deux marteaux, l'un en or, l'autre en
argent.

Les recherches dans le lac ont révélé l'existence d'une ville, noyée
lors d'un tremblement de terre.

Les plongeurs ont retrouvé de l'or, de l'argent.

Des récipients en bronze.

Dans l'eau, le bronze brillait.

On aurait dit de l'or.

Ils ont cru que c'était de l'or pur, ils avaient envie de mordre dedans
pour savoir si c'était de l'or.

Ces objets ont deux mille ans.

Les cruches sont pleines de sable.

Plus on s'enfonce vers le sol pour chercher le trésor, plus les pensées
s'envolent vers le ciel.

Certains endroits sont sacrés.

L'énergie y est palpable.

À l'endroit où l'on cherche le trésor, près de la falaise, on peut
ressentir cette énergie.

Un aigle vole.

C'est un bon signe.

Un aigle qui vole au-dessus d'un homme est le signe que ce qu'il fait
est juste.

Maintenant on parle d'une œuvre collective monumentale, fondatrice, plus
longue que l'Odyssée.

Les conteurs se transmettent la fable, ils connaissent des milliers de
vers par cœur.

La femme chante les vers en se balançant, levant un bras, puis l'autre
et psalmodiant les chants épiques.

Ils viennent de l'union du soleil et de la lune.

Ils racontent l'union de l'or et de l'argent.

Le ciel et les nuages sont des rivières qui tombent.

Ces chants sont peut-être tombés du ciel.

Le plus cruel des jeux est le loup gris.

Deux équipes s'affrontent autour d'une carcasse de mouton sans tête.

Ils s'affrontent, se disputent, crachent au sol.

Ils entraînent les chevaux pour la compétition.

Ils sont mécaniciens, ils participent aux courses à cheval.

Les mères reçoivent les invités, elles couvrent les tables de bonbons
pour honorer les visiteurs.

La tradition dit que les jeunes hommes doivent enlever une jeune fille
pour se marier avec elle.

L'enlèvement des jeunes filles se perpétue.

Les larmes le jour des noces sont synonymes de mariage heureux, dit le
dicton.

C'est une loterie, dit la mère.

Parfois ça marche, parfois ça ne marche pas.

Bien sûr j'ai résisté comme toute jeune fille, mais mes parents sont
arrivés et ils ont donné mon accord à ma place.

Les chemins de terre deviennent impraticables en hiver.

Parfois on se sent seul.

On dit que le cheval est aussi les ailes de l'homme.

Il donne de la viande, de quoi boire, de quoi se déplacer.

Lorsque le destin aura fini de jouer avec moi, dit la femme, ne pleure
pas, ne sois pas triste, car je laisse une source d'eau pure.

C'est le chant que j'ai écrit et je crois que les montagnes seront
reconnaissantes.

Parce que je les aime et que cet amour est réciproque.

Un jour devant la sculpture du David, les gardiens ont vu la femme
s'affaisser.

Elle était envoûtée.

De près on voit les veines, je ne plaisante pas.

On le trouvait parfait.

On oublie que parfois la beauté peut être quelque chose d'effrayant et
de tétanisant.

Un autre jour, quelqu'un est entré dans le musée avec un marteau pour
lui marteler le pied.

Que la réaction soit positive ou négative, c'est la perte des limites.

L'objet n'est plus un objet, c'est quelque chose d'autre qui pourrait
être doté d'une vie.

On en arrive presque à penser à son appartenance à une autre catégorie
d'objet.

Il y a une lumière, un halo, comme sur une scène de théâtre, la lumière
en direction des vedettes sur la scène.

Je me suis senti physiquement regardé par les femmes qui étaient sur ces
peintures, dit-il.

J'ai eu un effet de vertige.

L'univers propre de chacun des tableaux débordait à l'intérieur de la
salle.

Je ne savais pas exactement comment redescendre doucement, comment
réatterrir dans le réel.

La peinture est une fenêtre ouverte sur la fiction.

J'ai juste vu quelque chose de très beau, qui me dépasse.

Une sensation de malaise.

Cela fait émerger le premier émerveillement.

Nous sommes capturés.

Une toile pour immobiliser.

Pour emprisonner.

Les hennissements des chevaux peints.

Les bruits de sabots des chevaux peints.

Quelqu'un sait.

Le ravissement est un rapt.

Vous décrivez un élément caché.

Un détail, presque invisible à l'œil, émerge.

Le genou de Narcisse avance vers nous.

Vous quittez la réalité mais vous vous cognez dans le réel.

Ce dont je me souviens est très petit.

Noir comme une auge.

Un sarcophage.

Tout vient de l'enfance, vous ne saviez pas ?

L'or de leur corps est peint.

J'ai mal à l'estomac.

La belle Angèle, je ne peux pas la regarder, je pleure.

Je ne sais pourquoi je ne peux pas la regarder.

Ce n'est pas possible.

Je n'arrive pas à soutenir son regard peint, dit-il en pleurant devant
le tableau.

J'aimerais comprendre ce qui provoque cela, dit-il debout devant le
fleuve.

Si tu restes immobile, la patrouille ne te voit pas.

Mais si tu vas jusqu'à la mer, elle t'emporte.

Maintenant, les gens luttent pour survivre.

Pas pour vivre.

Pour survivre.

Il n'y a plus de hérons sur ces pilotis.

Il y a un camion et des monticules de déchets.

Il y a cette vieille dame, à moitié ensevelie sous les poubelles.

Elle ramasse les bouteilles en verre pour quelques pièces.

Le peintre ne cherche pas de nouvelles techniques.

Ses images sont simples, peintes sur des plaques d'aluminium.

Il peint des retables.

Pendant qu'il dessine, il écoute les témoignages.

La vieille femme ensevelie.

Une autre vieille femme a été menottée, les chevilles enchaînées, parce
qu'elle n'avait pas de papiers.

Il peint les menottes et les chaînes, et il écrit son histoire sur le
mur qui la sépare de ses petits-enfants.

Je vois où vous voulez en venir.

Vous voulez sous-entendre que je suis pour moitié un personnage fictif.

Il faut pas croire toutes ces histoires qu'on raconte.

On s'esquinte à courir derrière l'image originale.

Mais l'image originale est déjà là.

Elle est au fond de la rétine de l'œil.

Et sur cette image-là, il n'y a pas encore de copyright.

L'exploration reprend.

On essaie d'abord de trouver les bons récifs.

On étudie les cartes aussi.

On parle avec les gens du coin.

Après c'est une question de chance.

Le récif est une tour de Babel qui abrite des milliers d'espèces
différentes.

Les êtres vivants interagissent.

Chaque espèce a sa place dans ce monde.

Nous arpentons ce terrain comme des géomètres.

Chaque bloc semble s'imbriquer dans son voisin.

Des formes étranges n'existent nulle part ailleurs sur la planète.

Dans la lumière de l'aube, les voyageurs saisissent des formes de chaque
côté de la voie.

La terre fournit le matériau de base.

Il faut sourire ?

Non, surtout pas.

Regarde-moi.

J'aime bien mettre les mains dans les poches.

Je voudrais que tu me fasses un sourire.

Je ne saurais pas décrire ce sourire.

Une petite expression de fierté.

Ça va comme ça ?

Je voudrais qu'on fasse un tour d'horizon complet de la lumière et de la
couleur.

Tu peux imaginer tous les problèmes inimaginables qui t'arrivent.

Vous comprenez vos droits ?

Vous avez des questions ?

Non, rien du tout.

Donc, vous comprenez vos droits.

Nous allons procéder à l'inspection.

Les mains dans le dos.

On dirait qu'ils sont arrivés.

Ce n'est pas correct.

Ce sera un monde de souffrance.

L'aéroport fourmille d'activité.

Chaque bagage est inspecté.

Nous pouvons établir si un bagage est contaminé.

Nos scanners peuvent fournir une image en deux ou en trois dimensions.

Le risque de blessures graves est trop élevé.

Partons au nord, toujours plus au nord.

Il n'y a rien de spécifique à avoir vu.

Tout est dans les détails.

Un peintre découvre le sujet idéal.

Il était où il devait être, quand il devait y être.

C'est quelqu'un de spécial, excentrique, solitaire.

C'était quelqu'un qui fixait les gens, mais vous ne vous en rendiez pas
compte.

Il était détesté et méprisé par tout le monde.

Sauf par les gens qui aimaient ceux qui sont un peu dérangés.

C'est quand même ce qui compte dans l'art, refléter la vie réelle.

Il nous fait voir les choses différemment.

Je n'utilise pas de liants.

Je suis un homme simple qui utilise des matériaux simples, dit-il.

Je crois pas qu'il ait jamais peint une ombre.

Il ne pouvait pas se frayer un chemin.

Ma mère étendait le linge le lundi.

Le soir, elle le retrouvait aussi sale que lorsqu'elle l'avait entré
dans la machine.

C'est une représentation.

On voit très bien ça.

Tous les gens qui l'ont rencontré sont sans doute là-dedans.

Il y a en eux une vraie solitude.

Chacun marche dans son petit monde propre.

J'étais aussi comme ça quand j'étais jeune, perdu dans mon monde, et
c'était plutôt calme.

On passe l'essentiel de nos vies à attendre.

C'est ainsi que se comportent les humains.

C'est ainsi que fonctionnent les foules.

Tout le monde est en mouvement tout le temps, non ?

Tout le monde marche.

Tout le monde est un peu courbé.

Il y a un petit chien décharné.

Mes personnages, ce sont des vraies gens.

La foule est l'endroit le plus solitaire qui soit.

Chacun est un étranger pour les autres.

Je passe mon temps à me demander : mais quelle vie ont-ils ?

Une étrange fourmilière, pleine de choses fascinantes.

Il cherchait des endroits laids.

Il disait : voici un endroit réel.

Il attirait l'attention.

Il y a souvent quelque chose de théâtral dans la composition.

Ces immeubles ont l'air artificiels.

Ils font ainsi de ce lieu une vraie scène de théâtre.

C'est un peu comme s'asseoir au balcon du théâtre, à regarder une pièce.

Il se passe toujours quelque chose quand on observe un tableau et qu'on
réalise que le tableau vous observe à son tour.

Dans la pantomime, les personnages parlent en permanence au public.

Il se tourne vers moi et il me dit : les gens pensent que je ne peins
que des tordus.

Tu vois ce que je veux dire ?

C'est le combat de la vie.

Une bagarre, ça a un fort pouvoir d'attraction.

Il a une réelle affection pour les gens.

Il ne les regarde pas de haut, pas du tout.

Il est resté un homme calme qui m'a laissé vivre ma vie.

Ma mère avait un goût très sûr et collectionnait les horloges anciennes.

J'avais peu d'amis.

Je ne réussissais jamais aux examens.

On m'a conseillé une orientation artistique, parce que je n'arrivais à
rien.

À huit ans, je dessinais des bateaux.

C'est pour cela que je suis devenu peintre.

Jamais peint en situation.

Toujours de mémoire.

Je pense que c'est intemporel.

Les humains auront toujours le même problème.

Gardez ce coude en haut.

Plus haut.

Encore plus haut.

Je cherche des indices de ce qu'il était.

Il y a tellement d'indices qui disent tout du personnage.

Son endurance.

Le courage qu'il avait quand personne ne pouvait l'aider.

Il est dur de savoir s'il s'agissait de quelqu'un de réel.

Je ne sais pas.

Elle, elle semble prête à dévorer la vie.

Maintenant c'est trop tard pour commencer.

Elle manque horriblement de bonté.

À croire que cet homme n'a connu la bonté nulle part.

Plus elle se détournait de lui, plus il cherchait son attention.

Elle le critiquait en permanence.

Il a été dévasté quand elle est morte.

La clé, c'est ce qu'on ne voit pas.

Les gens trouvaient ses tableaux difficiles.

Ils voulaient de l'évasion.

Vendu.

On passe maintenant au premier lot.

La cote est plus élevée que jamais.

342, félicitations, monsieur.

Merci beaucoup.

Les collectionneurs sont surtout des hommes.

En général, ils en collectionnent plus d'un, comme s'ils ne pouvaient
pas s'arrêter.

J'achète des tableaux parce qu'ils me plaisent.

Et je les mets sur mes murs.

Il n'y a absolument rien ici, c'est un endroit sinistre.

Je passe beaucoup de temps à me demander ce que je suis venu y faire.

Deux bouteilles de lait tous les jours.

Parce qu'il aimait boire du lait.

Je crois qu'il se sentait seul.

Mais je crois que c'est ainsi qu'il voulait vivre.

Je crois que son talent était un poids très lourd à porter.

J'aime les horloges.

Elles sont toutes réglées sur différentes heures.

Mais on s'y habitue.

Parfois il avait de la peinture blanche sur ses vêtements.

Il plantait au hasard une punaise sur une carte, parce qu'il ne savait
pas où aller.

Si vous faites un tour dans les îles, vous verrez ses tableaux partout.

La profondeur est là, la distance, le climat, tout est là.

Il prenait des choses parfaitement normales et les rendait étranges.

Sa vie a été ruinée.

Elle ne s'en est jamais remise.

Ils sont différents.

Et les gens différents peuvent connaître le succès, pour n'importe
quelle raison.

Ils se faisaient un peu peur.

Ce ne sont pas de vraies créatures, ce sont des rêves, c'est pour ça que
je les aime.

Il était comme une personne emprunte de magie, vous savez ?

Et il ne faisait que ce dont il avait envie.

Cette exposition fut un très grand succès.

Il mourut juste avant que cela n'arrive.

Un jour je suis venu avec mon lait.

Ils étaient en train de tout emporter.

C'est la dernière fois que j'ai vu ses tableaux.

Ses enveloppes, son courrier.

Vous voulez les ouvrir ?

Ça ressemble à un vrai lieu et pas à une composition.

Tout au long de sa vie, il a été considéré comme un outsider, et ça n'a
pas changé.

Les tableaux sont à la cave.

Ou alors c'est parce qu'il venait du nord ?

Est-ce que quelqu'un sait pourquoi ?

Est-ce qu'il y a une raison officielle ?

Il ne s'intègre pas.

Ça lui permet de représenter des idoles.

Elle apporte un soutien moral qui est très important.

Sans elle je suis à la rue, dit-il.

Toutes les questions qu'il se pose, il me les pose rapidement, dit-elle.

Tu vois on a les mêmes goûts.

Regarde, c'est beau ces tons-là ?

Ça marche ?

Elle est plus artiste que moi.

Je l'aide à coller.

Il faut aller très vite.

Je suis trop lente apparemment.

On n'a pas le droit d'être un petit peu de travers.

Vite ! Vite ! Brosse !

En fait je n'ai pas le droit à l'erreur, tout simplement.

Le but du travail, c'est quand même que j'arrache tout.

Un avis féminin est hyper intéressant.

On négocie.

Ça me permet de sortir de ma zone de confort.

Il réfléchit encore plus.

Le côté féminin en art amène quelque chose.

Je l'écoute beaucoup.

C'est des contraintes.

Et il le dit toujours, les contraintes c'est bien, parce que ça permet
d'améliorer.

Je vais signer, dit-il, au cas où je meurs.

J'estime que c'est terminé quand je vois vraiment une assise.

Faut que ça vibre.

Faut que ça tienne la route.

J'aime bien me laisser surprendre.

J'aime bien découvrir ce que je fais avec une certaine latence.

Que ce soit incontrôlable.

Si tout est maîtrisé, si tout est contrôlé, on va finir par se faire
rudement chier.

La première urgence.

Faut le faire dans la journée.

En comptant surtout sur l'équilibre.

Après : le visage de fin.

Après : les déchirures.

Je lui ai dit : je peux pas le faire.

Il m'a dit : si, tu vas le faire.

Une peinture de l'exil.

Il a du mal avec l'engagement politique.

Il veut pas se positionner là-dessus.

Il n'a voulu représenter que des enfants.

Le regard de ces enfants, ce sera l'œuvre.

Ce qu'on verra de loin, ce sera les bouilles de ces gosses qui sont
vraiment victimes.

Il a carte blanche.

C'est sa vision, son regard.

Moi je suis pas un artiste engagé.

Mais moi j'aime m'engager dans des causes qui me touchent beaucoup.

J'entends des femmes me dire qu'elles ont quitté la mort pour aller vers
la mort.

En hommage, je ne vais pas changer ma manière de peindre.

Quelque chose de simple.

C'est juste un message d'amour, rien d'autre.

Quand on lui a proposé le mur, il a dit : oui.

C'est pas rien, c'est lourd de sens, c'est difficile d'y aller.

Il y a des gens qui viennent prier, il y a des bougies.

C'est un mur qui s'est fait dans la tristesse.

Il est très fier, mais il ne s'en est pas vanté plus que ça.

Avant sa mort, c'est bouleversant.

Il y a des couleurs, ça dégouline, et à certains endroits il n'y en a
plus.

Il n'y a plus rien à inventer.

On a tout vu.

On prend du recul.

On ne se réjouit pas trop trop vite.

Vivre au jour le jour, et puis voir à plus long terme, ça me suffit.

Par moment, vous ne comprenez pas les questions.

J'ai eu la chance inouïe d'habiter un immeuble avec un peintre au
rez-de-chaussée.

J'écoutais tout ce qui se disait.

J'ai commencé sur papier, sur bois.

Les fées créatrices se sont penchées sur lui, dit-elle.

Avec admiration.

Peindre sur les murs, sur les cabines téléphoniques, sur les frigos.

J'ai vingt ans.

Lors d'un festival, il se couvre le corps de peinture blanche.

J'ai inventé un personnage de trapéziste bondissant, qui escalade, qui
monte sur des cordes.

J'ai vu.

J'ai dit : ça c'est moi.

J'essaie de réunir tout ça.

De la terre, des matières, une écriture au tableau, avec tous les
reliefs.

Dedans, on a le temps.

On peut se concentrer.

Dehors, on ne peut pas.

Dehors, on ne peut pas retoucher.

Quand je dessine une tête, faut que je voie tout le corps.

Ça prend de la place les ailes, on n'y pense pas.

On peut les classer.

Les images d'amour d'un côté.

Les images infernales de l'autre.

Il veut faire triompher l'universalité de son alter ego.

C'est une question de survivance.

Peindre les maisons qui vont être démolies, c'est interdit.

Ce qui devrait être interdit, c'est d'expulser des gens.

Il va dans toutes les villes.

Il est systématique.

Son travail est construit.

On fera les petits détails après.

Faudrait des pinceaux plus petits.

Il inscrit dans l'histoire un message léger, loin des clichés de
violence actuels.

Ici, on a le tablier qui peut accueillir une voie de chemin de fer ou
une route.

J'enlève le tablier pour qu'on voie mieux.

C'est très léger.

Les architectes ont dit : je peux construire un bâtiment avec ça.

Je prends un demi-pont et je le colle sur un côté.

Je fais la même chose de l'autre côté.

On voit tout de suite que ça crée un espace ouvert.

Et c'est exactement ce que les bâtisseurs ont fait.

Six poutres en treillis sont tout d'abord reliées à une maçonnerie
centrale.

Les variations de température risquent de provoquer un problème.

Le toit risque de s'arracher de ses fixations et de s'effondrer.

Pour éviter une catastrophe, on attache des chariots à roulettes à la
base de chaque poutre.

Cela permet au bâtiment de se dilater sans s'effondrer.

On pourrait presque dire : enlevez vos chaussures en entrant, c'est un
lieu sacré.

Un toit trop lourd peut s'effondrer sous sa propre charge.

Les poutres se composent toutes de tubes creux.

Le plexiglas est un matériau idéal pour les cockpits de bombardiers, dit
l'homme.

Une femme se pare de bijoux.

Elle est passée par une phase beaucoup plus ingrate.

Elle n'a pas de longues pattes, pas de vert vif, mais ne la sous-estimez
pas, elle sait déjà donner la mort.

Elle se creuse un terrier douillet, un cylindre parfait.

Quand il fera trop chaud, elle le fermera avec un couvercle, mais pas
tout de suite.

Avant, elle veut piéger une victime, dit la femme.

Elle ne laisse que ses yeux à la surface.

Et attend.

Le bon moment.

Sa patience est récompensée.

Elle peut rester là un an.

Les montagnes ont formé les dunes et se chargent également de leur
donner à boire.

Les eaux des fontes dévalent les pentes.

C'est imprévisible et soudain.

L'eau se faufile dans les dunes, puis s'évacue dans les plaines.

La montagne rend les armes.

On doit s'inquiéter de prédateurs, plus lents, mais tout aussi
formidables.

Les plus âgées sont vulnérables.

Les blessées également.

Comment savoir qui l'emporterait lors d'une embuscade.

Le coyote s'éloigne.

Il n'y a qu'une ligne de protection efficace.

Repérer l'intrus avant d'être vu.

Agir comme un leurre.

Le coyote connaît les règles du jeu.

Il perd l'avantage de la surprise.

Les oiseaux migrateurs se suivent dans le ciel, espacés régulièrement.

Des lignes, des boucles, des lettres ouvertes.

Ce qui se dessine disparaît l'instant d'après.

Bientôt c'est une avalanche.

Les ailes couvrent le ciel.

Ils envahissent la vallée.

Ils s'arrêtent ici depuis des millions d'années.

Ils ont des affaires sérieuses à traiter.

Ils se sont réunis en groupes de survies.

L'adresse.

Il avait sept ans.

Il a fui, il est revenu vingt ans après.

C'est la continuité.

Heureusement qu'il y a toujours ces petites choses qui continuent.

Dans la réserve, une surprise de taille.

Encore un trésor dans la ville.

L'entrée authentique, l'entrée originale d'une église.

Cette porte était l'entrée principale avant la destruction du site, un
riche héritage, dit-il.

Cette maison appartient à la famille de mon père.

Ils sont arrivés il y a à peu près neuf ans.

À chaque fois que je passe par là, je sens que ce sont mes racines.

Il ne reste que quelques minutes avant l'exposition.

Dans ces pièces, tu pourras imaginer qu'il y a avait de l'eau, de la
vapeur, du chaud, du froid, on enterrait la vie des jeunes femmes, une vraie festivité.

C'est très symbolique.

Une inscription qui veut dire : liberté.

Pour mettre toutes les chances de ton côté, il ne faut pas hésiter à
prendre quelques risques.

Le plafond est creusé de trous ronds, en forme de fleurs géantes, qui
donnent sur le ciel et laissent passer la lumière.

L'idée, c'est de faire du bien.

L'idée, c'est la continuité de la vie.

Nous devons creuser, faire des installations, nous devons tout
restaurer.

Nous devons prendre en considération notre rapport au corps.

On continue à explorer.

J'aimerais beaucoup voir plein d'eau ici, une fontaine, de l'eau jaillir
de partout.

Des enfants crient dans la cour.

Ils sont des milliers à se rendre sur l'esplanade.

Ils encerclent un dôme doré, rigoureusement gardé par la police.

C'est un jour béni.

L'ordre ordonne.

L'ordre dit : les gens discutent ensemble, c'est un devoir social.

Ça fait partie de l'éducation.

Les images s'exposent, c'est pas toujours facile, il y a des
contre-jours, il y a des endroits sombres, ça raconte beaucoup de choses, des fondations, des mouvements, vrais ou faux, officiels ou pas, révolutionnaires ou non.

De nombreuses affiches de propagande sont réalisées.

De qui cette œuvre est-elle l'autoportrait ?

Dans quelle ville ?

Voilà une question.

Attention, ne faites pas les mêmes erreurs.

Ne détournons pas le regard quand la mécanique se grippe.

On est comme un chasseur à l'affût.

On immortalise.

Où je vais ?

Ça frise l'exploit.

Pour moi, cette démonstration est une réussite.

Pari gagné.

Elle est à vous.

Quelle allure.

Elle est à la hauteur de mes espérances.

Regarde.

Soyez prêts à vous battre ou à fuir comme jamais.

Le soleil se couche sur la mer.

Le port est endormi.

En hiver, des milliers de chalutiers arrivent.

Les quelques usines encore en activité finissent de traiter leurs
productions.

Des monticules de poissons séchés, gueules ouvertes, entassés comme des
morceaux de bois.

Partons retrouver ceux qui ont choisi de revenir vivre sur la terre de
leurs ancêtres.

Montagnes, plaines, gel, lande.

Je ressens quelque chose de particulier pour cet endroit.

Ici, c'est comme ma maison.

C'est mon chez-moi.

Cette zone a toujours été très chère à la culture.

Si vous venez ici, regardez ces pierres, on ne sait pas à quoi elles
servent, peut-être pour apprivoiser les animaux.

J'ai la chair de poule quand je passe ici.

Troupeaux de rennes, jumelles, guetteurs.

Le chef de meute observe les humains.

Puis ils détalent tous, en montrant leurs fesses blanches.

Il y a huit saisons, dit-elle.

Les autres ont quatre saisons, nous nous en avons huit, dit-elle.

Les rennes suivent le cycle des saisons.

Ce n'est pas à eux de nous suivre, c'est à nous, dit-elle.

C'est ainsi depuis des milliers d'années.

On est très occupés, il faut aller dans les montagnes. Manquent les dunes.

Il faut faire ce voyage plusieurs fois pendant l'été.

On a beaucoup de chance de pouvoir vivre comme ça.

On vit avec la nature, dans la nature, de la nature.

Fondu enchaîné.

Bateau brise-glace.

Poursuivons notre route.

Dirigeons-nous vers la capitale du nord, dit-il.

Salon-cabine avec vue sur la mer.

Sauna sur le pont du bateau.

Sommets enneigés.

Proue, homme à béret, sac à dos, église triangulaire.

Les aventures des grands explorateurs résonnent encore.

En visite dans ce centre, on peut observer les jeux des otaries.

Appareils photo, familles.

Une grue domine les maisons rouges.

Elle se reflète à la surface de l'eau, dans les fenêtres, dit-elle.

Nous entreprenons la dernière partie de ce voyage.

Les lumières du port se font de plus en plus petites.

Un avion clignote dans la nuit.

Le commandant de bord note quelque chose dans un grand cahier.

On a un incident en pleine montagne, dit-il.

Il faut absolument intervenir maintenant.

Votre métier est unique, dit-elle.

Des centaines de sites pour trouver le lieu qu'il me faut.

Et, une fois sur place, ce sera à moi de jouer.
