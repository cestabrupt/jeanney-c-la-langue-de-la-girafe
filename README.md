# ~/ABRÜPT/C. JEANNEY/LA LANGUE DE LA GIRAFE/*

La [page de ce livre](https://abrupt.ch/c-jeanney/la-langue-de-la-girafe/) sur le réseau.

## Sur le livre

Ce livre est un antilivre. Il est une multitude. Il s’entend [en vidéo](https://www.youtube.com/watch?v=Hv48c67OI68), se voit [en sons](https://soundcloud.com/cestabrupt/la-langue-de-la-girafe-c-jeanney), se contemple dans [sa dynamique numérique](https://txt.abrupt.ch/antilivre/abrupt-jeanney-c-la-langue-de-la-girafe-antilivre-dynamique.html).

*La langue de la girafe* est une écologie poétique du langage, un art du moindre. Dans ce texte dont la trame se structure telle une tapisserie, l’image appartient à l’errance, elle dérive le long des mythes, interroge l’acte même du faire.

« Le monde est rempli de textes, plus ou moins intéressants ; je n’ai aucune envie de lui en ajouter un de plus. » Cet ouvrage de C. Jeanney prend au mot l’affirmation de Kenneth Goldsmith et fait œuvre de *patchwork*.

C. Jeanney y a récolté les voix qui oscillaient autour d’elles, le verbe du quotidien, des paroles radiophoniques aux mots éphémères du passant, et les a assemblées en un collage de la langue.

La façon dont ces mots s’assemblent imite l’appareil photographique qui collectionne le fugace. Elle provoque l’émergence d’une sorte d’*infra-sens*, d’une texture narrative qui laisse surgir des messages souterrains à la langue.

Les mots sont déjà présents dans le réel, foisonnants et bariolés, le plus souvent ignorés, ils flottent insensément, et *La langue de la girafe* tâche de voltiger dans leur collecte protectrice. La langue s’exprime pleinement par elle-même. Elle ne souhaite pas sa réinvention, mais invite à l’infini de sa recomposition.

## Sur l'auteur

C. Jeanney est autrice, parfois traductrice, souvent artiste visuelle. Elle fait ce qu’elle a à faire, et ce qu’elle a fait parle pour elle. Pas besoin de chercher beaucoup plus.

Et ce qu’elle a fait ou fait ou fera se trouve notamment sur son site [tentatives](http://christinejeanney.net/). [La suite a six minutes](https://lasuiteasixminutes.com/) y fait suite. La [maison\[s\]témoin](http://www.maisonstemoin.fr/author/cjeanney/) aussi.

Il est possible de la suivre sur [facebook](https://www.facebook.com/christine.jeanney.16) et [twitter](https://twitter.com/cjeanney). La liste de ses ouvrages se trouve sur [la page bibliographique de son site](http://christinejeanney.net/spip.php?article939).

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
